<?php


namespace common\servers\chat\models;


class ChatServerCommand
{
    const REGISTER = 'register';
    const MESSAGE = 'message';
    const READ = 'read';
    const ISWRITING = 'isWriting';

    const MESSAGE_READ = 1;
    const MESSAGE_UNREAD = 0;
}