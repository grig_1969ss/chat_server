<?php

namespace common\servers\chat;

use common\servers\chat\helpers\UserData;
use common\servers\chat\models\ChatServerCommand;
use common\servers\chat\models\CommandEvent;
use common\servers\chat\models\NewMessageCommand;
use common\servers\chat\models\RegisterCommand;
use consik\yii2websocket\events\WSClientMessageEvent;
use consik\yii2websocket\WebSocketServer;
use Ratchet\ConnectionInterface;

class ChatServer extends WebSocketServer
{
    /**
     * @var array
     * array of connections by name
     */
    private $chatClients = [];

    /**
     * @var array
     *
     * connectedUsers is an array with key
     * ConnectionInterface object name
     * and value User id(integer)
     *
     */
    private $connectedUsers = [];

    /**
     * @return array
     */
    public function getChatClients(): array
    {
        return $this->chatClients;
    }

    /**
     * @param $connectionName
     * @param $senderConnection
     */
    public function setChatClients($connectionName, $senderConnection): void
    {
        $this->chatClients[$connectionName] = $senderConnection;
    }

    /**
     * @return array
     */
    public function getConnectedUsers(): array
    {
        return $this->connectedUsers;
    }

    /**
     * @param $connectionName
     * @param $senderId
     */
    public function setConnectedUsers($connectionName, $senderId): void
    {
        $this->connectedUsers[$connectionName] = $senderId;
    }

    /**
     * @param $conn_name
     * @return bool
     */
    public function getClientByConnectionName($conn_name): bool
    {
        return array_key_exists($conn_name, $this->connectedUsers);
    }

    /**
     * @param  $conn_name
     */
    public function removeClientByConnectionName($conn_name): void
    {
        if (array_key_exists($conn_name, $this->connectedUsers)) {
            unset($this->connectedUsers[$conn_name]);
        }
        if (array_key_exists($conn_name, $this->chatClients)) {
            unset($this->chatClients[$conn_name]);
        }
    }

    /**
     * @param string $id
     * @return bool|int|string
     */
    public function getConnectionNameByUserId(string $id)
    {
        foreach ($this->connectedUsers as $connectionName => $connectedUser) {
            if($connectedUser == $id) {
                return $connectionName;
            }
        }
        return false;
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function getConnectionByUserId(string $id)
    {
        $name = $this->getConnectionNameByUserId($id);
        return $name ? $this->getConnectionByName($name) : false;
    }


    /**
     * @param $conn_name
     * @return object|null
     */
    public function getConnectionByName($conn_name)
    {
        return $this->chatClients[$conn_name] ?? null;
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->removeClientByConnectionName($conn->resourceId);

        parent::onClose($conn);
    }

    /**
     * Call all functions
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_CLIENT_MESSAGE, function (WSClientMessageEvent $e) {
            $data = json_decode($e->message, true);

            /**
             * getting user auth keys for saving messages in mongodb
             */
            $data = UserData::prepareReceiverData($data);

            /**
             * get sender, receiver, and message variables, if receiver exists else register sender
             */
            $command = $data['command'];

            /**
             * @var CommandEvent $commandEvent
             */
            $commandEvent = new CommandEvent($this, $e->client, $data);

            /**
             * Call the current command's event
             */
            if ($command == ChatServerCommand::REGISTER) {
                $commandEvent->registerNewClient();
            } elseif ($command == ChatServerCommand::MESSAGE) {
                $commandEvent->registerNewMessage();
            } elseif ($command == ChatServerCommand::READ) {
                $commandEvent->registerNewRead();
            } elseif($command == ChatServerCommand::ISWRITING) {
                $commandEvent->registerIsWriting();
            }

        });
    }
}
